console.log('Задание 1');

function Student(name, point) {
    this.name = name;
    this.point = point;
}

Student.prototype.show = function () {
    console.log('Студент %s набрал %d баллов', this.name, this.point);
};

var student = new Student('Вася Пупкин', 10);
student.show();

console.log('Задание 2');

function  StudentList(group, students) {

    this.group = group;
    if (students instanceof Array && students.length > 0) {
        for (var i = 0; i < students.length - 1; i += 2) {
            var newStudentsName = students[i];
            var newStudentPoint = students[i + 1];
            this.add(newStudentsName, newStudentPoint);
        }
    }
}

//StudentList.prototype = Array.prototype;

StudentList.prototype = Object.create(Array.prototype);

StudentList.prototype.add = function(newStudentName, newStudentPoint) {
    var newStudent = new Student(newStudentName, newStudentPoint);
    this.push(newStudent);
};

console.log('Задание 3');

var studentsAndPoints = [
    'Алексей Петров', 0,
    'Ирина Овчинникова', 60,
    'Глеб Стукалов', 30,
    'Антон Павлович', 30,
    'Виктория Заровская', 30,
    'Алексей Левенец', 70,
    'Тимур Вамуш', 30,
    'Евгений Прочан', 60,
    'Александр Малов', 0
];

var hj2 = new StudentList('«HJ-2»', studentsAndPoints);

console.log(hj2);

console.log('Задание 4');

hj2.add('Иван Петров', 10);
hj2.add('Петр Иванов', 20);
hj2.add('Анджелина Джоли', 30);
hj2.add('Дмитрий Медведев', 10);

console.log(hj2);

console.log('Задание 5');

var html7 = new StudentList('«HTML-7»');

html7.add('Сергей Бобровский', 10);
html7.add('Илья Сорокин', 20);
html7.add('Игорь Шестёркин', 40);
html7.add('Виктор Антипин', 20);
html7.add('Егор Аверин', 20);
html7.add('Александр Овечкин', 20);
html7.add('Мария Шурочкина', 50);
html7.add('Светлана Ромашева', 30);
html7.add('Наталья Ищенко', 80);

console.log(html7);

console.log('Задание 6');

StudentList.prototype.show = function() {
    console.log('Группа %s (%d студентов):', this.group, this.length);
    this.forEach(function(student){
        student.show();
    });
};

hj2.show();

html7.show();

console.log('Задание 7');

StudentList.prototype.moveStudent = function(name, newGroup) {
    if (!(newGroup instanceof StudentList)) {
        console.log('Новая группа должна быть экземпляром StudentList!');
        return false;
    }
    var studentPosition = this.findIndex(function(student) {
        return student.name == name;
    });

    if (studentPosition == -1) {
        console.log('Студент %s в группе %s не найден', name, this.group);
        return false;
    }

    var students = this.splice(studentPosition, 1);
    console.log(students);
    newGroup.push(students[0]);
    console.log('Студент %s перемещен из группы %s в группу %s', students[0].name, this.group, newGroup.group);
};

html7.moveStudent('Егор Аверин1', hj2); // 'Не найден'
html7.moveStudent('Егор Аверин', hj2);
html7.moveStudent('Егор Аверин', {}); // 'Не экземпляр группы'
hj2.moveStudent('Егор Аверин', html7);

console.log('Дополнительное задание');

Student.prototype.valueOf = function() {
    return this.point;
};

StudentList.prototype.max = function() {
    var maxBall = Math.max.apply(Math, this);
    return this.find(function(item) {
       return item.point == maxBall;
    });
};

var maxStudent = hj2.max();
if (maxStudent instanceof Student) {
    maxStudent.show();
}
maxStudent = html7.max();
if (maxStudent instanceof Student) {
    maxStudent.show();
}

//[1, 2, 3].show();
